#include "window.hpp"

int main(int argc, char* argv[])
{
    Window win(800, 600, "3D Game Engine");
    win.useDefaultInputHandler();
    win.run();

    return 0;
}
