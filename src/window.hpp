#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <functional>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Window
{
public:
    Window(int width, int height, std::string title) {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        m_window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
        if (m_window == NULL) {
            //Error
            glfwTerminate();
        }

        glfwMakeContextCurrent(m_window);
        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
            //Error
        }

        glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, int width, int height) {
            glViewport(0, 0, width, height);
        });
    }

    void setInputHandler(std::function<void (GLFWwindow*)> handler) {
        m_inputHandler = handler;
    }

    void setRenderHandler() {}

    void useDefaultRenderHandler() {}

    void useDefaultInputHandler() {
        std::function<void (GLFWwindow*)> inputHandler = [](GLFWwindow* window) {
            if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
                glfwSetWindowShouldClose(window, true);
        };

        setInputHandler(inputHandler);
    }

    void run() {
        while(!glfwWindowShouldClose(m_window)) {
            m_inputHandler(m_window);

            glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            glfwSwapBuffers(m_window);
            glfwPollEvents();
        }
    }

    ~Window() {
        glfwDestroyWindow(m_window);
        glfwTerminate();
    }

private:
    std::function<void (GLFWwindow*)> m_inputHandler;
    GLFWwindow* m_window;
};

#endif // WINDOW_HPP
